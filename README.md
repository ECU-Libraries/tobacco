# ECU Pitt County Tobacco Digital Exhibit

Jekyll site for the legacy ECU Pitt County Tobacco digital exhibit.
* [http://digital.lib.ecu.edu/exhibits/tobacco](http://digital.lib.ecu.edu/exhibits/tobacco)

## Modification

Modification requires building the site with [Jekyll](https://jekyllrb.com/), which requires Ruby.

1. Clone repo
2. $ cd steamers
3. $ bundle install
4. $ bundle exec jekyll serve

This should make [http://localhost:4000/exhibits/tobacco/](http://localhost:4000/exhibits/tobacco/) available in your browser to view the site (trailing slash is important here). After changes are made, copy the contents of the _site directory to the live webserver.

